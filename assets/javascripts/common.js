/* スクロールして発生 */
$(function() {
  $(window).on('touchmove scroll', function() {
    if ($(this).scrollTop() > 80) {
      $('header').addClass('header-active');
    } else {
      $('header').removeClass('header-active');
    }
    if ($(this).scrollTop() > 300) {
      $('.pagetop').addClass('pagetop-active');
    } else {
      $('.pagetop').removeClass('pagetop-active');
    }
  });
});


/* スクロール */
$(function() {
  var pagetop = $('.pagetop');
  pagetop.click(function () {
    $('html, body').animate({ scrollTop: 0 }, 700);
        return false;
  });
});


/* カテゴリー別一覧表示 高さをあわせる */
$(function(){
  if ($('.categories ul li').css('margin-bottom') == '40px') {
    var setHeight = $('.categories ul li:nth-of-type(9)');
    $(window).load(function(){
      var listHeight01 = $('.categories ul li:nth-of-type(8)').css('height');
      var listHeight02 = $('.categories ul li:nth-of-type(10)').css('height');
      var getHeight = parseInt(listHeight01) + parseInt(listHeight02) + 40;
      setHeight.css({'height':getHeight});
    });
  };
});

/* フォームチェック */
$(document).ready(function() {
  $('#othersCheck').removeAttr('checked');
  $('#othersAns').attr('disabled', 'disabled');
  $('#othersCheck').click(function(){
      if($(this).prop('checked') == true){
           $('#othersAns').removeAttr('disabled');
      }
      else {
          $('#othersAns').prop('disabled', 'disabled');
      }
  });
})

/* コメント欄開閉 */
$(document).ready(function() {
  var positionComment = $('#comment-list li:last-of-type dl').offset().top;
  $('div.moreread a').click(function () {
    $('div.moreread').fadeOut(100);
    $('.collapse-comment-list .comment-list').animate({'padding-top':'0px'}, 700);
  });
  $('div.moreclose a').click(function () {
    $('div.moreread').fadeIn();
    $('.collapse-comment-list .comment-list').css('padding-top', '40px');
    $('html, body').animate({ scrollTop: positionComment - 200 });
  });
})